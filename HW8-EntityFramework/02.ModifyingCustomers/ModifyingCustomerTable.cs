﻿using System;
using System.Collections.Generic;
using System.Linq;
using _01.CreateNorthwindModel;

namespace _02.ModifyingCustomers
{
    public class ModifyingCustomerTable
    {
        static void Main()
        {
            // Add new customer
            Customer customer = new Customer();
            customer.CompanyName = "Telerik Academy";
            customer.CustomerID = customer.CompanyName.Substring(0,3).ToUpper() + customer.CompanyName.Substring(6,2).ToUpper();
            customer.ContactName = "Svetlin Nakov";
            customer.ContactTitle = "Trainers team";
            customer.Address = "Forsterstr. 57";
            customer.City = "Sofia";
            customer.PostalCode = "445t55";
            customer.Country = "Bulgaria";
            customer.Phone = "+3590258879728";
            AddCustomer(customer);

            Customer customer2 = new Customer()
            {
                CompanyName = "New company",
                CustomerID = "NEWCO"
            };
            AddCustomer(customer2);

            // Modify customer company name
            ModifyCustomer("NEWCO", "ChangedCompany");

            // Delete added customer
            DeleteCustomer(customer.CustomerID);
            DeleteCustomer("NEWCO");

        }

        public static void AddCustomer(Customer customer)
        {
            using (NorthwindEntities northwindEntity = new NorthwindEntities())
            {
                northwindEntity.Customers.Add(customer);
                northwindEntity.SaveChanges();

                Console.WriteLine("{0} added.", customer.CompanyName);
            }
        }

        public static void ModifyCustomer(string customerId, string newCompanyName)
        {
            using (NorthwindEntities northwindEntity = new NorthwindEntities())
            {
                Customer customer = SelectCustomerByID(customerId, northwindEntity);
                customer.CompanyName = newCompanyName;
                northwindEntity.SaveChanges();

                Console.WriteLine("{0} modified.", customer.CompanyName);
            }
        }

        public static void DeleteCustomer(string customerId)
        {
            using (NorthwindEntities northwindEntity = new NorthwindEntities())
            {
                Customer customer = SelectCustomerByID(customerId, northwindEntity);
                northwindEntity.Customers.Remove(customer);
                northwindEntity.SaveChanges();

                Console.WriteLine("{0} deleted.",customer.CompanyName);
            }
        }

        public static Customer SelectCustomerByID(string customerID, NorthwindEntities northwindEntity)
        {
            var customer = northwindEntity.Customers.FirstOrDefault(x => x.CustomerID == customerID);
            //northwindEntity.Customers.Find(customerId);
            return customer;
        }

    }
}
