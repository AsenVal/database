﻿using _01.CreateNorthwindModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10.CreateStoredProcedureForTotalIncomeForSupplier
{
    class CreateStoredProcedureForTotalIncomeForSupplier
    {
        static void Main()
        {
            using (NorthwindEntities context = new NorthwindEntities())
            {
                CreateUsp_FindTotalIncome();
                var totalIncome = GetTotalIncomes("Tokyo Traders", new DateTime(1990, 1, 1), new DateTime(2000, 1, 1));

                Console.WriteLine(totalIncome);
            }
        }

        private static void CreateUsp_FindTotalIncome()
        {
            using (NorthwindEntities northwindEntites = new NorthwindEntities())
            {
                string sqlProcedure =
                    @"drop procedure [dbo].[usp_FindTotalIncome]";
                    northwindEntites.Database.ExecuteSqlCommand(sqlProcedure);

                    sqlProcedure = @"CREATE PROC usp_FindTotalIncome (@startDate datetime, @endDate DateTime, @companyName nvarchar(50))
                    AS
                    SELECT SUM(od.Quantity*od.UnitPrice) AS TotalIncome
                    FROM Suppliers s
                    INNER JOIN Products p
                    ON s.SupplierID = p.SupplierID
                    INNER JOIN [ORDER Details] od
                    ON od.ProductID = p.ProductID
                    INNER JOIN Orders o
                    ON od.OrderID = o.OrderID
                    WHERE s.CompanyName = @companyName AND (o.OrderDate >= @startDate AND o.OrderDate <= @endDate)";
                northwindEntites.Database.ExecuteSqlCommand(sqlProcedure);
            }
        }

        static decimal? GetTotalIncomes(string supplierName, DateTime? startDate, DateTime? endDate)
        {
            using (NorthwindEntities northwindEntites = new NorthwindEntities())
            {
                // run one time and remve below comment
                var totalIncomeSet = northwindEntites.usp_FindTotalIncome(startDate, endDate, supplierName);

                foreach (var totalIncome in totalIncomeSet)
                {
                    return totalIncome;
                }
            }

            return null;
        }
    }
}
