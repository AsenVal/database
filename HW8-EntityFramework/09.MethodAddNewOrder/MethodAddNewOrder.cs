﻿using _01.CreateNorthwindModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace _09.MethodAddNewOrder
{
    class MethodAddNewOrder
    {
        static void Main()
        {
            Order newOrder = new Order();
            newOrder.CustomerID = "FRANK";
            newOrder.EmployeeID = 5;
            newOrder.OrderDate = new DateTime(2012, 6, 30);
            newOrder.RequiredDate = new DateTime(2012, 8, 22);
            newOrder.ShippedDate = new DateTime(2012, 8, 12);
            newOrder.ShipVia = 3;
            newOrder.Freight = 20.05M;
            newOrder.ShipName = "Cargo ship";

            Order newOrder2 = new Order();
            newOrder2.CustomerID = "FRANK";
            newOrder2.EmployeeID = 5;
            newOrder2.OrderDate = new DateTime(2012, 6, 30);
            newOrder2.RequiredDate = new DateTime(2012, 8, 22);
            newOrder2.ShippedDate = new DateTime(2012, 8, 12);
            newOrder2.ShipVia = 3;
            newOrder2.Freight = 20.05M;
            newOrder2.ShipName = "Cargo ship";

            Console.WriteLine("{0} records changed", AddOrder(new List<Order>(){ newOrder }));
            Console.WriteLine("{0} records changed", AddOrder(new List<Order>() { newOrder2 , newOrder}));

        }

        public static int AddOrder(List<Order> newOrders)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                using (var dataBase = new NorthwindEntities())
                {
                    int result = 0;
                    try
                    {
                        foreach (var newOrder in newOrders)
                        {
                            //var fefe =  dataBase.Orders.Where(c => c.CustomerID == "FRANK").ToList();
                            var customer = dataBase.Customers.First(c => c.CustomerID == newOrder.CustomerID);
                            if (customer != null)
                            {
                                newOrder.ShipAddress = customer.Address;
                                newOrder.ShipCity = customer.City;
                                newOrder.ShipPostalCode = customer.PostalCode;
                                newOrder.ShipCountry = customer.Country;
                            }

                            dataBase.Orders.Add(newOrder);
                            result += dataBase.SaveChanges();
                        }

                        tran.Complete();
                        return result;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
            }
        }
    }
}
