﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Data.EntityClient;
using _01.CreateNorthwindModel;

namespace _06.CreateDatabaseCopy
{
    class CreateDatabaseCopy
    {
        static void Main()
        {
            IObjectContextAdapter dataBase = new NorthwindEntities();
            string cloneNorthwind = dataBase.ObjectContext.CreateDatabaseScript();
            string createNorthwindCloneDB = "USE master; " +
                                            "CREATE DATABASE NorthwindTwin; " +
                                            "SELECT name, size, size*1.0/128 AS [Size in MBs] " +
                                            "FROM sys.master_files " +
                                            "WHERE name = N'NorthwindTwin'; ";

            SqlConnection dbCon = new SqlConnection("Server=.\\SQLEXPRESS; " +
                                                    "Database=master; Integrated Security=true");
            dbCon.Open();
            using (dbCon)
            {
                SqlCommand createCloneDB = new SqlCommand(createNorthwindCloneDB, dbCon);
                createCloneDB.ExecuteNonQuery();
                string changeDB = "USE NorthwindTwin; ";
                SqlCommand changeDataB = new SqlCommand(changeDB, dbCon);
                changeDataB.ExecuteNonQuery();
                SqlCommand cloneDB = new SqlCommand(cloneNorthwind, dbCon);
                cloneDB.ExecuteNonQuery();
            }
        }
    }
}
