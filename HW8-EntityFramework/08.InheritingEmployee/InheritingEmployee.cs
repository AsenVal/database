﻿using _01.CreateNorthwindModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _08.InheritingEmployee
{
    public class InheritingEmployee
    {
        static void Main()
        {
            Employee extended = new Employee();
            
            NorthwindEntities context = new NorthwindEntities();

            extended = context.Employees.Find(1);

            foreach (var item in extended.EntityTerritories)
            {
                Console.WriteLine("Teritory description - {0}", item.TerritoryDescription);
            }
        }
    }
}
