﻿using System;
using System.Collections.Generic;
using System.Linq;
using _01.CreateNorthwindModel;
using System.Data.Linq;

namespace _08.InheritingEmployee
{
    public partial class NoEmployee // this class must be put in the model
    {
        private EntitySet<Territory> entityTerritories;

        public EntitySet<Territory> EntityTerritories
        {
            get
            {
                var employeeTerritories = this.EntityTerritories;
                EntitySet<Territory> entityTerritories = new EntitySet<Territory>();
                entityTerritories.AddRange(employeeTerritories);
                return entityTerritories;
            }
        }
    }
}
