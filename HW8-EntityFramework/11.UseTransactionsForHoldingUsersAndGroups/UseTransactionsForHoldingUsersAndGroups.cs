﻿using _11.CreateUsersDBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace _11.UseTransactionsForHoldingUsersAndGroups
{
    class UseTransactionsForHoldingUsersAndGroups
    {
        static void Main()
        {
            for (int i = 0; i < 2; i++)
            {
                if (CreateUser("Milcho", "Ivanov", "MilchoIvanov", "Milcho80", "Admin"))
                {
                    Console.WriteLine("User added successfully.");
                }
            }
        }


        private static bool CreateUser(string fname, string lname, string userName, string pass, string group)
        {
            User newUser = new User();
            newUser.FirstName = fname;
            newUser.LastName = lname;
            newUser.UserName = userName;
            newUser.UserPassword = pass;
            Group newGroup = new Group();
            newGroup.GroupName = group;

            using (var contextDB = new UserDBEntities())
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    try
                    {
                        if (contextDB.Groups.FirstOrDefault(x => x.GroupName == newGroup.GroupName) == null)
                        {
                            contextDB.Groups.Add(newGroup);
                            contextDB.SaveChanges();
                        }
                        var groupID = contextDB.Groups.First(x => x.GroupName == newGroup.GroupName);
                        newUser.GroupID = groupID.GroupID;
                        contextDB.Users.Add(newUser);
                        contextDB.SaveChanges();
                        trans.Complete();
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() != typeof(System.Data.UpdateException))
                        {
                            Console.WriteLine("An error occured. "
                                + "The operation cannot be retried.\n"
                                + ex.Message);
                        }
                        return false;
                    }
                }

            }

            return true;
        }
    }
}

/*
// Use this to create UserDB
CREATE DATABASE UserDB
GO

USE UserDB
CREATE TABLE Groups (
 GroupID INT IDENTITY
  CONSTRAINT UNIQUE_GROUP_ID UNIQUE,
 GroupName NVARCHAR(50),

 CONSTRAINT PK_GROUP_ID PRIMARY KEY (GroupID)
)

GO

USE UserDB
CREATE TABLE Users (
 UserID INT IDENTITY
  CONSTRAINT UNIQUE_USER_ID UNIQUE,
 FirstName NVARCHAR(50),
 LastName NVARCHAR(50),
 UserName NVARCHAR(50) NOT NULL
   CONSTRAINT UNIQUE_UserName UNIQUE,
 UserPassword NVARCHAR(50) NOT NULL,
 GroupID INT NOT NULL

 CONSTRAINT PK_UserID PRIMARY KEY (UserID) 
 CONSTRAINT FK_Users_Groups FOREIGN KEY (GroupID) REFERENCES Groups(GroupID)
 )

GO
*/