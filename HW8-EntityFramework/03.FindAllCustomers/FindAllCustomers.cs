﻿using System;
using System.Collections.Generic;
using System.Linq;
using _01.CreateNorthwindModel;

namespace _03.FindAllCustomers
{
    class FindAllCustomers
    {
        static void Main()
        {
            using (NorthwindEntities northwindEntity = new NorthwindEntities())
            {
                /*northwindEntity.Customers.Join
                    (northwindEntity.Orders,
                    (c => c.CustomerID), 
                    (o => o.CustomerID), 
                    (c,o) => new {c, o.OrderDate, o.ShipCountry})
                    .Where(x => x.ShipCountry == "Canada").Where(x => x.OrderDate.Value.Year == 1997).Select(x => x.ContactName).Distinct().ToList().ForEach(f => Console.WriteLine(f));*/

                var orders = northwindEntity.Orders.Where(x => x.ShipCountry == "Canada" && x.OrderDate.Value.Year == 1997).Select(c => c.Customer).Distinct().ToList();
                var customers = northwindEntity.Customers.Where(o => o.Orders.Any(x => x.ShipCountry == "Canada" && x.OrderDate.Value.Year == 1997)).ToList();

                foreach (var item in customers)
                {
                    Console.WriteLine(item.ContactName);
                }
            }
        }
    }
}
