﻿using _01.CreateNorthwindModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _04.FindAllCustomersWithSQLRequest
{
    class FindAllCustomersWithSQLRequest
    {
        static void Main()
        {
            var customers = SelectCustomerByDataAndCountry("Canada", 1997).ToList();
        }

        private static IEnumerable<string> SelectCustomerByDataAndCountry(
        string country, int year)
        {
            NorthwindEntities northwindEntities = new NorthwindEntities();
            string nativeSqlQuery =
                "SELECT DISTINCT c.* FROM Customers c " + 
                "JOIN Orders o " +
                "ON c.CustomerID = o.CustomerID " +
                "WHERE Datepart(year, o.OrderDate) = " + year + " " +
                "AND o.ShipCountry = '" + country + "'";
            object[] parameters = { country, year };
            var customers = northwindEntities.Database.SqlQuery<string>(
                nativeSqlQuery, parameters);
            return customers;
        }
    }
}
