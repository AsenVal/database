﻿using _01.CreateNorthwindModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;

namespace _05.FindAllSalesByRegionAndPeriod
{
    class FindAllSalesByRegionAndPeriod
    {
        static void Main()
        {
            var sales = FindSalesByPeriodAndRegion("WA", new DateTime(1997, 01, 01), new DateTime(1997, 06, 06));
            foreach (var sale in sales)
            {
                Console.WriteLine(sale);
            }
        }

        public static IEnumerable FindSalesByPeriodAndRegion(string region, DateTime startDate, DateTime endDate)
        {
            using (NorthwindEntities northwindEntity = new NorthwindEntities())
            {
                var sales = northwindEntity.Sales_by_Year(startDate, endDate)
                    .Join(northwindEntity.Order_Details,
                    (s => s.OrderID),
                    (o => o.OrderID),
                    (s, o) => new { ShipName = o.Order.ShipName, Sales = o.Quantity, OrderDate = o.Order.OrderDate, o.Order.ShipRegion })
                    .Where(x => x.ShipRegion == region).ToList();

                var askedSales = (from order in northwindEntity.Orders
                                  join orderDetails in northwindEntity.Order_Details
                                  on order.OrderID equals orderDetails.OrderID
                                  where (order.ShipRegion == region && order.OrderDate.Value >= startDate && order.ShippedDate.Value <= endDate)
                                  //where (order.ShipRegion == region && EntityFunctions.TruncateTime(order.OrderDate.Value) >= startDate.Date && EntityFunctions.TruncateTime(order.ShippedDate.Value) <= endDate.Date)
                                  select new
                                  {
                                      ShipName = order.ShipName,
                                      Sales = orderDetails.Quantity,
                                      OrderDate = EntityFunctions.TruncateTime(order.OrderDate.Value)
                                  }).ToList();
                /*var askedSales = (from salesByYear in northwindEntity.Sales_by_Year(startDate, endDate)
                                 join orderDetails in northwindEntity.Order_Details
                                 on salesByYear.OrderID equals orderDetails.OrderID
                                 where (orderDetails.Order.ShipRegion == region)
                                 select new
                                 {
                                     ShipName = orderDetails.Order.ShipName,
                                     Sales = orderDetails.Quantity,
                                     OrderDate = orderDetails.Order.OrderDate
                                 }).ToList();*/
                return sales;
            }
        }
    }
}
