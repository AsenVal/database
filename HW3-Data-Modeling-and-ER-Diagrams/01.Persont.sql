USE [master]
GO
/****** Object:  Database [01.Persons]    Script Date: 7/13/2013 3:05:23 PM ******/
CREATE DATABASE [01.Persons]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'People', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\01.Persons.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'People_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\01.Persons_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [01.Persons] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [01.Persons].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [01.Persons] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [01.Persons] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [01.Persons] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [01.Persons] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [01.Persons] SET ARITHABORT OFF 
GO
ALTER DATABASE [01.Persons] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [01.Persons] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [01.Persons] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [01.Persons] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [01.Persons] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [01.Persons] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [01.Persons] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [01.Persons] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [01.Persons] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [01.Persons] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [01.Persons] SET  DISABLE_BROKER 
GO
ALTER DATABASE [01.Persons] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [01.Persons] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [01.Persons] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [01.Persons] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [01.Persons] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [01.Persons] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [01.Persons] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [01.Persons] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [01.Persons] SET  RESTRICTED_USER 
GO
ALTER DATABASE [01.Persons] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [01.Persons] SET DB_CHAINING OFF 
GO
ALTER DATABASE [01.Persons] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [01.Persons] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [01.Persons]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 7/13/2013 3:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AddressText] [nvarchar](50) NOT NULL,
	[TownId] [int] NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Continent]    Script Date: 7/13/2013 3:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Continent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_Continent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 7/13/2013 3:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[ContinentId] [int] NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person]    Script Date: 7/13/2013 3:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[AddressId] [int] NOT NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Town]    Script Date: 7/13/2013 3:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Town](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_Town] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([Id], [AddressText], [TownId]) VALUES (1, N'Al. Malinov', 1)
INSERT [dbo].[Address] ([Id], [AddressText], [TownId]) VALUES (2, N'Rakocska', 1)
INSERT [dbo].[Address] ([Id], [AddressText], [TownId]) VALUES (3, N'Main Sreet', 5)
SET IDENTITY_INSERT [dbo].[Address] OFF
SET IDENTITY_INSERT [dbo].[Continent] ON 

INSERT [dbo].[Continent] ([Id], [Name]) VALUES (1, N'Asia')
INSERT [dbo].[Continent] ([Id], [Name]) VALUES (2, N'Australia')
INSERT [dbo].[Continent] ([Id], [Name]) VALUES (3, N'Europa')
INSERT [dbo].[Continent] ([Id], [Name]) VALUES (4, N'Africa')
INSERT [dbo].[Continent] ([Id], [Name]) VALUES (5, N'America')
SET IDENTITY_INSERT [dbo].[Continent] OFF
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([Id], [Name], [ContinentId]) VALUES (1, N'Bulgaria', 3)
INSERT [dbo].[Country] ([Id], [Name], [ContinentId]) VALUES (2, N'Germany', 3)
INSERT [dbo].[Country] ([Id], [Name], [ContinentId]) VALUES (3, N'USA', 5)
INSERT [dbo].[Country] ([Id], [Name], [ContinentId]) VALUES (4, N'Libia', 4)
INSERT [dbo].[Country] ([Id], [Name], [ContinentId]) VALUES (5, N'China', 1)
INSERT [dbo].[Country] ([Id], [Name], [ContinentId]) VALUES (6, N'Ausralia', 2)
SET IDENTITY_INSERT [dbo].[Country] OFF
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([Id], [FirstName], [LastName], [AddressId]) VALUES (1, N'Ivan', N'Stoyanov', 1)
INSERT [dbo].[Person] ([Id], [FirstName], [LastName], [AddressId]) VALUES (2, N'George', N'Petrov', 2)
INSERT [dbo].[Person] ([Id], [FirstName], [LastName], [AddressId]) VALUES (3, N'Brian', N'Adams', 3)
SET IDENTITY_INSERT [dbo].[Person] OFF
SET IDENTITY_INSERT [dbo].[Town] ON 

INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (1, N'Sofia', 1)
INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (2, N'Berlin', 2)
INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (3, N'Plovdiv', 1)
INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (4, N'Varna', 1)
INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (5, N'Boston', 3)
INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (6, N'NewYork', 3)
INSERT [dbo].[Town] ([Id], [Name], [CountryId]) VALUES (7, N'Sidny', 6)
SET IDENTITY_INSERT [dbo].[Town] OFF
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Town] FOREIGN KEY([TownId])
REFERENCES [dbo].[Town] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Town]
GO
ALTER TABLE [dbo].[Country]  WITH CHECK ADD  CONSTRAINT [FK_Country_Continent] FOREIGN KEY([ContinentId])
REFERENCES [dbo].[Continent] ([Id])
GO
ALTER TABLE [dbo].[Country] CHECK CONSTRAINT [FK_Country_Continent]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK_Person_Address] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK_Person_Address]
GO
ALTER TABLE [dbo].[Town]  WITH CHECK ADD  CONSTRAINT [FK_Town_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[Town] CHECK CONSTRAINT [FK_Town_Country]
GO
USE [master]
GO
ALTER DATABASE [01.Persons] SET  READ_WRITE 
GO
