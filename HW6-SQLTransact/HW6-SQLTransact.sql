-- Task1
-- Create a database with two tables: Persons(Id(PK), FirstName, LastName, SSN) and Accounts(Id(PK), PersonId(FK), Balance). 
-- Insert few records for testing. Write a stored procedure that selects the full names of all persons.

CREATE DATABASE Bank
GO

USE Bank
CREATE TABLE Persons (
 PersonID INT IDENTITY
  CONSTRAINT UNIQUE_PERSON_ID UNIQUE,
 FirstName NVARCHAR(50) NOT NULL,
 LastName NVARCHAR(50) NOT NULL,
 SSN NVARCHAR(10) NOT NULL

 CONSTRAINT PK_PersonID PRIMARY KEY (PersonID)
)

GO

CREATE TABLE Accounts (
 AccountID INT IDENTITY
  CONSTRAINT UNIQUE_ACCOUTN_ID UNIQUE,
 PersonID INT NOT NULL,
 Balance MONEY NOT NULL

 CONSTRAINT PK_ID PRIMARY KEY (AccountID)
 CONSTRAINT FK_Accounts_Persons FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
)

GO

INSERT INTO Persons (FirstName, LastName, SSN) VALUES ('Pesho', 'Ivanov', '1655867890')
INSERT INTO Persons (FirstName, LastName, SSN) VALUES ('Ivaylo', 'Petrov', '6912345544')
INSERT INTO Persons (FirstName, LastName, SSN) VALUES ('Georgi', 'Georgiev', '9876543520') 

INSERT INTO Accounts (PersonID, Balance) VALUES (1, 5500.44)
INSERT INTO Accounts (PersonID, Balance) VALUES (2, 12200.78)
INSERT INTO Accounts (PersonID, Balance) VALUES (3, 1345.09)

GO

CREATE PROCEDURE usp_SelectFullName
	AS
		SELECT CONCAT(FirstName, ' ', LastName) AS FullName FROM Persons
	GO

EXEC usp_SelectFullName
GO

-- Task2
-- Create a stored procedure that accepts a number as a parameter and returns all persons 
-- who have more money in their accounts than the supplied number.

CREATE PROC usp_GetPersonsWithBalanceGreaterThan (@minBalance MONEY)	
	AS
		SELECT CONCAT(FirstName, ' ', LastName) AS FullName FROM Persons p
		JOIN Accounts a
		ON a.PersonID = p.PersonID
		WHERE a.Balance > @minBalance
	GO

EXEC  usp_GetPersonsWithBalanceGreaterThan 5000
GO

-- Task3
--  Create a function that accepts as parameters � sum, yearly interest rate and number of months. 
-- It should calculate and return the new sum. Write a SELECT to test whether the function works as expected.

CREATE FUNCTION usp_CalculateInterestRate (@sumMoney MONEY, @intRate REAL, @months INT)
RETURNS MONEY
AS
BEGIN
	RETURN (@sumMoney * @intRate/100 * @months/12)
END
GO

SELECT dbo.usp_CalculateInterestRate (4500, 9.5, 72) AS InterestRate
GO

-- Task4
-- Create a stored procedure that uses the function from the previous example to give an interest to a person's account
-- for one month. It should take the AccountId and the interest rate as parameters.

CREATE PROCEDURE usp_GetInterestRate(@accID INT, @intRate REAL)
	AS
	   DECLARE @sum MONEY 
	   SELECT @sum = Balance FROM Accounts  WHERE AccountID = @accID 
	   UPDATE Accounts
	   SET Balance += (SELECT dbo.usp_CalculateInterestRate(@sum, @intRate, 1))
	   WHERE AccountID = @accID
	GO

EXEC usp_GetInterestRate 1,10
GO

-- Task5
-- Add two more stored procedures WithdrawMoney( AccountId, money) and 
-- DepositMoney (AccountId, money) that operate in transactions.

CREATE PROCEDURE usp_WithDrawMoney(@accID INT, @moneySum MONEY)
	AS
	 BEGIN TRAN
	   DECLARE @NewBalance MONEY
	   SET @NewBalance = (SELECT Balance FROM Accounts
	   WHERE AccountID = @accID)  - @moneySum
	   IF (@NewBalance < 0)
	   BEGIN
		  ROLLBACK TRAN
	   END
	   ELSE
	   BEGIN	
	   UPDATE Accounts	
	   Set Balance = @NewBalance
	   WHERE AccountID = @accID  
		  COMMIT TRAN
	   END
	GO

	--ALTER TABLE Accounts
	--ADD CONSTRAINT PositiveBalance CHECK ((Balance) >= 0)
	--GO

EXEC usp_WithDrawMoney 1,10000
GO
EXEC usp_WithDrawMoney 1,1000
GO

CREATE PROCEDURE usp_DepositMoney(@accID INT, @moneySum MONEY)
	AS
	 BEGIN TRAN
	   UPDATE Accounts
	   SET Balance += @moneySum
	   WHERE AccountID = @accID
	 COMMIT TRAN
	GO

EXEC usp_DepositMoney 1,1000
GO

-- Task6
-- Create another table � Logs(LogID, AccountID, OldSum, NewSum). Add a trigger to the Accounts table 
-- that enters a new entry into the Logs table every time the sum on an account changes.

CREATE TABLE Logs (
 LogID INT IDENTITY
  CONSTRAINT UNIQUE_LOG_ID UNIQUE,
 OldSum int NOT NULL,
 NewSum int NOT NULL,
 AccountId int NOT NULL

 CONSTRAINT PK_LogID PRIMARY KEY (LogID)
 CONSTRAINT FK_Accounts_Logs FOREIGN KEY (LogID) REFERENCES Logs(LogID)
 )
 GO

 CREATE TRIGGER tr_Account ON Accounts FOR UPDATE
 AS  
      INSERT INTO Logs(OldSum, NewSum, AccountId)
	  SELECT d.Balance, i.Balance, d.AccountID
	  FROM deleted d
	  JOIN inserted i
	  ON d.AccountID = i.AccountID      
GO

-- Task 7 
-- Define a function in the database TelerikAcademy that returns all Employee's names (first or middle or last name) and
--  all town's names that are comprised of given set of letters. Example 'oistmiahf' will return 'Sofia', 'Smith', � but not 'Rob' and 'Guy'.
-- (regularExpresions for SQL http://www.codeproject.com/Articles/42764/Regular-Expressions-in-MS-SQL-Server-2005-2008)
sp_configure 'clr enabled', 1
GO
RECONFIGURE
GO
 
USE TelerikAcademy
CREATE ASSEMBLY
--assembly name for references from SQL script
SqlRegularExpressions
-- assembly name and full path to assembly dll,
-- SqlRegularExpressions in this case
FROM 'D:\4. T-SQL\SqlRegularExpressions.dll'  --change path to dll
WITH PERMISSION_SET = SAFE
GO
--function signature
CREATE FUNCTION RegExpLike(@Text nvarchar(MAX), @Pattern nvarchar(255)) RETURNS BIT
--function external name
AS EXTERNAL NAME SqlRegularExpressions.SqlRegularExpressions.[LIKE]
 
GO
 
CREATE FUNCTION fn_RegularExpressionFind ( @regularExpression nvarchar(30) )
RETURNS TABLE
AS
RETURN SELECT Emp.FirstName,
           Emp.MiddleName,
           Emp.LastName,
           Towns.Name
  FROM Employees AS Emp
  JOIN Addresses AS Addr
    ON Emp.AddressID = Addr.AddressID
  JOIN Towns
    ON Addr.TownID = Towns.TownID
 WHERE 1 = dbo.RegExpLike(LOWER(Towns.Name), @regularExpression)
   AND (1 = dbo.RegExpLike(LOWER(Emp.FirstName), @regularExpression)
    OR 1 = dbo.RegExpLike(LOWER(ISNULL(Emp.MiddleName, '')), @regularExpression)
        OR 1 = dbo.RegExpLike(LOWER(Emp.LastName), @regularExpression))
GO
 
SELECT * FROM fn_RegularExpressionFind('^[oistmiahf]+$')
 
-- Task 8
-- Using database cursor write a T-SQL script that scans all employees and their addresses and prints all pairs 
-- of employees that live in the same town.

DECLARE empCursor CURSOR READ_ONLY FOR
 
SELECT a.FirstName, a.LastName, t1.Name, b.FirstName, b.LastName
FROM Employees a
JOIN Addresses adr
ON a.AddressID = adr.AddressID
JOIN Towns t1
ON adr.TownID = t1.TownID,
 Employees b
JOIN Addresses ad
ON b.AddressID = ad.AddressID
JOIN Towns t2
ON ad.TownID = t2.TownID
WHERE t1.Name = t2.Name
  AND a.EmployeeID <> b.EmployeeID
ORDER BY a.FirstName, b.FirstName
 
OPEN empCursor
DECLARE @firstName1 NVARCHAR(50)
DECLARE @lastName1 NVARCHAR(50)
DECLARE @town NVARCHAR(50)
DECLARE @firstName2 NVARCHAR(50)
DECLARE @lastName2 NVARCHAR(50)
FETCH NEXT FROM empCursor
        INTO @firstName1, @lastName1, @town, @firstName2, @lastName2
 
WHILE @@FETCH_STATUS = 0
        BEGIN
                PRINT @firstName1 + ' ' + @lastName1 +
                        '     ' + @town + '      ' + @firstName2 + ' ' + @lastName2
                FETCH NEXT FROM empCursor
                        INTO @firstName1, @lastName1, @town, @firstName2, @lastName2
        END
 
CLOSE empCursor
DEALLOCATE empCursor
 
-- Task 9
-- * Write a T-SQL script that shows for each town a list of all employees that live in it. Sample output:

USE TelerikAcademy
DECLARE empCursor CURSOR READ_ONLY FOR
SELECT Name FROM Towns
OPEN empCursor
DECLARE @townName VARCHAR(50), @userNames VARCHAR(MAX)
FETCH NEXT FROM empCursor INTO @townName
 
 
WHILE @@FETCH_STATUS = 0
  BEGIN
                BEGIN
                DECLARE nameCursor CURSOR READ_ONLY FOR
                SELECT a.FirstName, a.LastName
                FROM Employees a
                JOIN Addresses adr
                ON a.AddressID = adr.AddressID
                JOIN Towns t1
                ON adr.TownID = t1.TownID
                WHERE t1.Name = @townName
                OPEN nameCursor
               
                DECLARE @firstName VARCHAR(50), @lastName VARCHAR(50)
                FETCH NEXT FROM nameCursor INTO @firstName,  @lastName
                WHILE @@FETCH_STATUS = 0
                        BEGIN
                                SET @userNames = CONCAT(@userNames, @firstName, ' ', @lastName, ', ')
                                FETCH NEXT FROM nameCursor
                                INTO @firstName,  @lastName
                        END
        CLOSE nameCursor
        DEALLOCATE nameCursor
                END
                SET @userNames = LEFT(@userNames, LEN(@userNames) - 1)
    PRINT @townName + ' -> ' + @userNames
    FETCH NEXT FROM empCursor
    INTO @townName
  END
CLOSE empCursor
DEALLOCATE empCursor
 
GO

--Task 10
-- Remove the aggregate and assembly if they're there

sp_configure 'clr enabled', 1
GO
RECONFIGURE
GO

IF OBJECT_ID('dbo.concat') IS NOT NULL DROP Aggregate concat
GO

IF EXISTS (SELECT * FROM sys.assemblies WHERE name = 'concat_assembly')
       DROP assembly concat_assembly;
GO     

CREATE Assembly concat_assembly
   AUTHORIZATION dbo
   FROM 'C:\Users\student\Desktop\Asen\HW6-SQLTransact\GenerateConcatAgregateFunction\GenerateConcatAgregateFunction\bin\Release\GenerateConcatAgregateFunction.dll'
   WITH PERMISSION_SET = SAFE;
GO

CREATE AGGREGATE dbo.concat (

    @Value NVARCHAR(MAX)
  , @Delimiter NVARCHAR(4000)

) RETURNS NVARCHAR(MAX)
EXTERNAL Name concat_assembly.concat;
GO 

SELECT dbo.concat(FirstName + ' ' + LastName, ', ')
FROM Employees  