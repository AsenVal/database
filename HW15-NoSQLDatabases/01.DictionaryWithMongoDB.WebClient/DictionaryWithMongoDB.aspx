﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DictionaryWithMongoDB.aspx.cs" Inherits="_01.DictionaryWithMongoDB.WebClient.DictionaryWithMongoDB" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Site.css" rel="stylesheet"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:Label CssClass="label" runat="server">Dictionary Word</asp:Label>
            <asp:TextBox ID="txtWord" CssClass="textbox" runat="server"></asp:TextBox><br />
            <asp:Label ID="Label1"  CssClass="label" runat="server">Dictionary Translation</asp:Label>
            <asp:TextBox ID="txtTranslation" CssClass="textbox" runat="server" Width="350px"></asp:TextBox><br />
            <asp:LinkButton ID="btnInsert" runat="server" OnClick="btnInsert_Click">Insert</asp:LinkButton>
            <asp:GridView ID="grdResult" CssClass="gridview" runat="server" OnRowCancelingEdit="grdResult_RowCancelingEdit" OnRowDeleting="grdResult_RowDeleting" OnRowEditing="grdResult_RowEditing" OnRowUpdating="grdResult_RowUpdating" DataKeyNames="_id">
                <Columns>
                    <asp:CommandField HeaderText="Action" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" />
                </Columns>
            </asp:GridView>
            <asp:Label ID="Label2"  CssClass="label" runat="server">Search By Word:</asp:Label>
            <asp:TextBox ID="txtSearch" CssClass="textbox" runat="server"></asp:TextBox>
            <asp:LinkButton ID="btnSearch" runat="server" OnClick="btnSearch_Click">Search</asp:LinkButton><br />
    </div>
    </form>
</body>
</html>
