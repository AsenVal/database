﻿using _01.DictionaryWithMongoDB.Data;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _01.DictionaryWithMongoDB.WebClient
{
    public partial class DictionaryWithMongoDB : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdResultFill();
            }
        }

        private void grdResultFill()
        {
            grdResult.DataSource = MongoDbProvider.db.LoadData<Dictionary>().ToList();
            grdResult.DataBind();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            Dictionary Dictionary = new Dictionary();
            Dictionary.Word = txtWord.Text;
            Dictionary.Translation = txtTranslation.Text;
            Dictionary.PublishDate = DateTime.Now;

            MongoDbProvider.db.SaveData(Dictionary);
            grdResultFill();
        }

        protected void grdResult_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            MongoDbProvider.db.DeleteData<Dictionary>(e.Keys[0].ToString());
            grdResultFill();
        }

        protected void grdResult_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            var id = new ObjectId(e.Keys[0].ToString());
            var Dictionary = MongoDbProvider.db.LoadData<Dictionary>().FirstOrDefault(b => b._id == id);
            if (Dictionary != null)
            {
                Dictionary.Word = e.NewValues[0] == null ? string.Empty : e.NewValues[0].ToString();
                Dictionary.Translation = e.NewValues[1] == null ? string.Empty : e.NewValues[1].ToString();
            }
            MongoDbProvider.db.SaveData(Dictionary);
            grdResult.EditIndex = -1;
            grdResultFill();
        }

        protected void grdResult_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            grdResult.EditIndex = e.NewEditIndex;
            grdResultFill();
        }

        protected void grdResult_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            grdResult.EditIndex = -1;
            grdResultFill();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSearch.Text))
            {
                grdResultFill();
            }
            else
            {
                grdResult.DataSource =
                MongoDbProvider.db.LoadData<Dictionary>().Where(b => b.Word.Contains(txtSearch.Text)).ToList();
                grdResult.DataBind();
            }
        }
    }
}