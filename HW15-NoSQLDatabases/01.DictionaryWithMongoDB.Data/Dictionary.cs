﻿using MongoDB.Bson;

namespace _01.DictionaryWithMongoDB.Data
{
    public class Dictionary
    {
        public ObjectId _id { get; set; }
        public string Word { get; set; }
        public string Translation { get; set; }
        public BsonDateTime PublishDate { get; set; }    
    }
}
