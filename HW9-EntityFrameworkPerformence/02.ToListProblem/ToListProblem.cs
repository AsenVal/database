﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.ToListProblem
{
    public class ToListProblem
    {
        static void Main()
        {
            TelerikAcademyEntities context = new TelerikAcademyEntities();
            PrintEmployeesFromSofia_Slow(context);
            PrintEmployeesFromSofia_Fast(context); // with only one ToList()
        }

        private static void PrintEmployeesFromSofia_Slow(TelerikAcademyEntities context)
        {
            var allEmployees = context.Employees.ToList()
                .Select(e => new
                {
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    TownName = e.Address.Town.Name
                }).ToList()
                .Where(e => e.TownName == "Sofia").ToList();
            foreach (var employee in allEmployees)
            {
                Console.WriteLine("Employee: {0} {1}, Town: {2}", employee.FirstName, employee.LastName, employee.TownName);
            }
        }

        private static void PrintEmployeesFromSofia_Fast(TelerikAcademyEntities context)
        {
            var allEmployeesOptimized = context.Employees
                .Select(e => new {
                        FirstName = e.FirstName,
                        LastName = e.LastName,
                        TownName = e.Address.Town.Name})
                .Where(e => e.TownName == "Sofia").ToList();
            foreach (var employee in allEmployeesOptimized)
            {
                Console.WriteLine("Employee: {0} {1}, Town: {2}",employee.FirstName, employee.LastName, employee.TownName);
            }

        }
    }
}
