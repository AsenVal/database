﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01.SQLQueryN_1Include
{
    public class SQLQueryN1Includerere
    {
        public static void Main()
        {
            TelerikAcademyEntities context = new TelerikAcademyEntities();
            PrintEmployeesWithQueryProblem(context); // slow with many querys
            PrintEmployeesWithInclude(context); // Fast with one query
        }

        public static void PrintEmployeesWithQueryProblem(TelerikAcademyEntities context)
        {
            foreach (var employee in context.Employees)
            {
                Console.WriteLine("Employee Name: {0} {1}, Department: {2}, Town: {3}", 
                    employee.FirstName, employee.LastName, employee.Department.Name, employee.Address.Town.Name);
            }
        }

        public static void PrintEmployeesWithInclude(TelerikAcademyEntities context)
        {
            foreach (var employee in context.Employees.Include("Department").Include("Address.Town"))
            {
                Console.WriteLine("Employee Name: {0} {1}, Department: {2}, Town: {3}",
                    employee.FirstName, employee.LastName, employee.Department.Name, employee.Address.Town.Name);
            }
        }
    }

    
}
