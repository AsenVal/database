-- Task4
SELECT * FROM Departments

-- Task5
SELECT Name FROM Departments

-- Task6
SELECT Salary FROM Employees

-- Task7
SELECT CONCAT(FirstName, ' ', LastName) FullName FROM Employees

-- Task8
SELECT CONCAT(FirstName, '.', LastName, '@telerik.com') [Full Email Address] FROM Employees

-- Task9 - different salaries
SELECT DISTINCT Salary FROM Employees

-- Task10
SELECT * FROM Employees e
WHERE JobTitle = 'Sales Representative'

-- Task11
SELECT FirstName, LastName FROM Employees
WHERE FirstName like 'SA%'

-- Task12
SELECT FirstName, LastName FROM Employees
WHERE LastName like '%ei%'

-- Task13
SELECT Salary FROM Employees e
WHERE Salary BETWEEN 20000 AND 30000

-- Task14
SELECT FirstName, LastName FROM Employees
WHERE Salary IN (25000, 14000, 12500, 23600)

-- Task15
SELECT FirstName, LastName, ManagerID FROM Employees
WHERE ManagerID IS NULL

-- Task16
SELECT FirstName, LastName, Salary FROM Employees
WHERE Salary > 50000
ORDER BY Salary DESC

-- Task17
SELECT TOP 5 FirstName, LastName, Salary FROM Employees
WHERE Salary > 50000
ORDER BY Salary DESC

-- Task18
SELECT FirstName, LastName, AddressText 
FROM Employees e
JOIN Addresses a 
ON e.AddressID = a.AddressID

-- Task19
SELECT FirstName, LastName, AddressText 
FROM Employees e, Addresses a
WHERE e.AddressID = a.AddressID

-- Task20
SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName,  CONCAT(m.FirstName, ' ', m.LastName) ManagerFullName
FROM Employees e, Employees m
WHERE e.ManagerID = m.EmployeeID

-- Task21
SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName,  CONCAT(m.FirstName, ' ', m.LastName) ManagerFullName, a.AddressText
FROM Employees e
JOIN Employees m
on e.ManagerID = m.EmployeeID
JOIN Addresses a
on e.AddressID = a.AddressID

-- Task 22
SELECT Name FROM Departments
UNION
SELECT Name FROM Towns

-- Task23
SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName,  CONCAT(m.FirstName, ' ', m.LastName) ManagerFullName
FROM Employees e
RIGHT JOIN Employees m
on e.ManagerID = m.EmployeeID

SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName,  CONCAT(m.FirstName, ' ', m.LastName) ManagerFullName
FROM Employees e
LEFT JOIN Employees m
on e.ManagerID = m.EmployeeID

-- Task24
SELECT CONCAT(e.FirstName, ' ', e.LastName) AS FullName, d.Name, e.HireDate
FROM Employees e
JOIN Departments d
ON e.DepartmentID = d.DepartmentID
WHERE d.Name IN('Sales', 'Finance') AND year(e.HireDate) BETWEEN 1995 AND 2001
