﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08.FindAllProductsThatContainGivenString
{
    class FindAllProductsThatContainGivenString
    {
        static void Main()
        {
            string searchedString = Console.ReadLine();
            //string output = Regex.Replace(userSearch, @"[^A-z0-9\-\s]*", "");
            string serverString = "Server=.\\SQLEXPRESS; Database=Northwind; Integrated Security=true";
            SqlConnection dbCon = new SqlConnection(serverString);
            List<string> allProducts = GetProducts(dbCon, searchedString);
            foreach (var item in allProducts)
            {
                Console.WriteLine(item);
            }
        }

        private static List<string> GetProducts(SqlConnection dbCon, string input)
        {
            List<string> allProducts = new List<string>();

            dbCon.Open();
            string sql = "SELECT ProductName FROM Products where ProductName LIKE @input";
            SqlParameter userInput = new SqlParameter("@input", "%" + input + "%");
            SqlCommand cmdAllRows = new SqlCommand(sql, dbCon);
            cmdAllRows.Parameters.Add(userInput);

            SqlDataReader reader = cmdAllRows.ExecuteReader();
            using (reader)
            {
                while (reader.Read())
                {
                    allProducts.Add((string)reader["ProductName"]);

                }
            }

            dbCon.Close();
            return allProducts;
        }
    }
}
