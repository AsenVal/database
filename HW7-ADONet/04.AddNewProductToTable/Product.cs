﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04.AddNewProductToTable
{
    public class Product
    {
        public string ProductName { get; set; }
        public int? SupplierID { get; set; }
        public int? CategoryID { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? UnitsInStock { get; set; }
        public int? UnitsOnOrder { get; set; }
        public int? ReorderLevel { get; set; }
        public bool Discontinued { get; set; }

        public Product(string productName, bool discontinued)
            :this(productName, null, null, null, null, null, null, null, discontinued)
        {
        }

        public Product(string productName, int? supplierID, int? categoryID, string quantityPerUnit, 
            decimal? unitPrice, int? unitsInStock, int? unitsOnOrder, int? reorderLevel, bool discontinued)
        {
            this.ProductName = productName;
            this.SupplierID = supplierID;
            this.CategoryID = categoryID;
            this.QuantityPerUnit = quantityPerUnit;
            this.UnitPrice = unitPrice;
            this.UnitsInStock = unitsInStock;
            this.UnitsOnOrder = unitsOnOrder;
            this.ReorderLevel = reorderLevel;
            this.Discontinued = discontinued;
        }
    }
}
