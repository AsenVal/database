﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04.AddNewProductToTable
{
    class AddNewProductToTable
    {
        static void Main()
        {
            string serverString = "Server=.\\SQLEXPRESS; Database=Northwind; Integrated Security=true";
            SqlConnection dbCon = new SqlConnection(serverString);

            Product spagetti = new Product("Spagetti", 2, 3, "300 g", 2.65m, 2, 5, 0, false);
            Product apple = new Product("Apples", false);
            AddProductToProductsDB(spagetti, dbCon);
            Console.WriteLine("Product {0} added to the database.", spagetti.ProductName);
            AddProductToProductsDB(apple, dbCon);
            Console.WriteLine("Product {0} added to the database.", apple.ProductName);
        }

        public static void AddParametar<T>(string variable, T param, ref SqlCommand command)
        {
            SqlParameter sqlParameter = new SqlParameter(variable, param);
            if (param == null)
            {
                sqlParameter.Value = DBNull.Value;
            }
            command.Parameters.Add(sqlParameter);
        }

        public static void AddProductToProductsDB(Product product, SqlConnection dbCon)
        {
            dbCon.Open();
            //using (dbCon)
            //{
                SqlCommand command = new SqlCommand(
                    "INSERT INTO Products" +
                    "(ProductName, SupplierID, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued) VALUES " +
                    "(@productName, @supplierID, @categoryID, @quantityPerUnit, @unitPrice, @unitsInStock, @unitsOnOrder, @reorderLevel, @discontinued)"
                , dbCon);
                AddParametar<string>("@productName", product.ProductName, ref command);
                AddParametar<int?>("@supplierID", product.SupplierID, ref command);
                AddParametar<int?>("@categoryID", product.CategoryID, ref command);
                AddParametar<string>("@quantityPerUnit", product.QuantityPerUnit, ref command);
                AddParametar<decimal?>("@unitPrice", product.UnitPrice, ref command);
                AddParametar<int?>("@unitsInStock", product.UnitsInStock, ref command);
                AddParametar<int?>("@unitsOnOrder", product.UnitsOnOrder, ref command);
                AddParametar<int?>("@reorderLevel", product.ReorderLevel, ref command);
                AddParametar<bool>("@discontinued", product.Discontinued, ref command);
                command.ExecuteNonQuery();
                dbCon.Close();
            //}
        }
    }
}
