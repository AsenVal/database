﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace _03.GetProductsInCategories
{
    class GetProductsInCategories
    {
        static void Main()
        {
            string serverString = "Server=.\\SQLEXPRESS; Database=Northwind; Integrated Security=true";
            SqlConnection dbCon = new SqlConnection(serverString);
            dbCon.Open();
            using (dbCon)
            {
                SqlCommand command = new SqlCommand(
                    "SELECT c.CategoryName, p.ProductName FROM Categories c " +
                    "Join Products p " +
                    "ON p.CategoryID = c.CategoryID", dbCon);
                SqlDataReader reader = command.ExecuteReader();

                Dictionary<string, List<string>> output = new Dictionary<string, List<string>>();
                using (reader)
                {
                    while (reader.Read())
                    { 
                        string categoryName = (string)reader["CategoryName"];
                        string productName = (string)reader["ProductName"];
                        if (output.ContainsKey(categoryName))
                        {
                            output[categoryName].Add(productName);
                        }
                        else
                        {
                            output.Add(categoryName, new List<string>());
                            output[categoryName].Add(productName);
                        }

                        foreach (var item in output)
                        {
                            Console.WriteLine("{0} - {1}", item.Key, string.Join(", ", item.Value));
                            Console.WriteLine();
                        }
                    }
                }
            }
        }
    }
}
