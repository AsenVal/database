﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Xml;
using System.Xml.Xsl;

namespace _07.InsertRecordsInExcel
{
    class InsertRecordInExcell
    {
        static void Main()
        {
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
           @"Data Source=..\..\ScoresTest.xlsx; Extended Properties=""Excel 12.0 Xml;HDR=YES""";

            OleDbConnection dbCon = new OleDbConnection(connectionString);

            CreateTable("Name", "Score", dbCon);
           
            InsertRow("Ivan Stoyanov", 56, dbCon);
            

        }

        public static void InsertRow(string name, double score, OleDbConnection dbCon)
        {
            dbCon.Open();
            OleDbCommand cmd = new OleDbCommand("INSERT INTO [Sheet1$] ([Name], [Score]) VALUES (@Name, @Score)", dbCon);
            cmd.Parameters.Add("@Name", name);
            cmd.Parameters.Add("@Score", score);
            cmd.ExecuteNonQuery();
            dbCon.Close();
        }

        public static void CreateTable(string name, string score, OleDbConnection dbCon)
        {
            dbCon.Open();
            OleDbCommand cmd = new OleDbCommand("CREATE TABLE [Sheet1] ([" + name + "] nvarchar(255), [" + score + "] number)", dbCon);
            cmd.ExecuteNonQuery();
            dbCon.Close();
        }
    
    }
}

