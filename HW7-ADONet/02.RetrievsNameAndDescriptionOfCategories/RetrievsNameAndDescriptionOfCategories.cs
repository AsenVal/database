﻿using System;
using System.Data.SqlClient;


namespace _02.RetrievsNameAndDescriptionOfCategories
{
    class RetrievsNameAndDescriptionOfCategories
    {
        static void Main()
        {
            string serverString = "Server=.\\SQLEXPRESS; Database=Northwind; Integrated Security=true";
            SqlConnection dbCon = new SqlConnection(serverString);
            dbCon.Open();
            using (dbCon)
            {
                SqlCommand command = new SqlCommand(
                    "SELECT CategoryName, Description FROM Categories", dbCon);
                SqlDataReader reader = command.ExecuteReader();

                //Console.WriteLine("Number of records in Category table");
                using (reader)
                {
                    while (reader.Read())
                    {
                        string categoryName = (string)reader["CategoryName"];
                        string description = (string)reader["Description"];
                        Console.WriteLine("{0} - {1}", categoryName, description);
                    }
                }
            }
        }
    }
}
