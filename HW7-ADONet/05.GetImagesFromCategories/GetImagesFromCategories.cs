﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;

namespace _05.GetImagesFromCategories
{
    class GetImagesFromCategories
    {
        static string ServerString = "Server=.\\SQLEXPRESS; Database=Northwind; Integrated Security=true";

        static void Main()
        {            
            SqlConnection dbCon = new SqlConnection(ServerString);

            // add new category record because pictures in records are broken
            //Image img = Image.FromFile("..\\..\\images\\download.jpg");
            //AddCategoryRecordDB(img, dbCon);

            dbCon.Open();
            using (dbCon)
            {
                SqlCommand command = new SqlCommand(
                    "SELECT Picture, CategoryID FROM Categories", dbCon);


                SqlDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        byte[] picture = (byte[])reader["Picture"];
                        int categoryid = (int)reader["CategoryID"];
                        //Image img = byteArrayToImage(picture);
                        //img.Save("..\\images\\" + categoryName + ".jpg");


                        var fs = new BinaryWriter(new FileStream(@"..\\..\\images\\" + categoryid + ".jpg", FileMode.Append, FileAccess.Write));
                        fs.Write(picture,78, picture.Length - 78);
                        fs.Close();
                        //
                    }
                }
            }
        }

        /*private static void ChangeCategoryImagesToDataBase()
        {
            SqlConnection dbCon = new SqlConnection(ServerString);
            dbCon.Open();
            using (dbCon)
            {
                SqlCommand command = new SqlCommand(
                    "UPDATE Categories " +
                    "SET Picture = (SELECT Picture FROM OPENROWSET (Bulk 'C:fdfe', SINGLE_BLOB) AS BLOB) " +
                    "WHERE CategoryID = 1", dbCon);
            }
        }*/
                
        public static void AddCategoryRecordDB(Image img, SqlConnection dbCon)
        {
            dbCon.Open();
            SqlCommand command = new SqlCommand(
                "INSERT INTO Categories" +
                "(CategoryName, Description, Picture) VALUES " +
                "(@categoryName, @description, @picture)"
            , dbCon);
            command.Parameters.AddWithValue("@categoryName", "ImageTest");
            command.Parameters.AddWithValue("@description", "Add image to test get images from database.");
            command.Parameters.AddWithValue("@picture", ImageToByteArray(img));
            command.ExecuteNonQuery();
            dbCon.Close();
        }
        
        /*public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }*/

        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

    }
}
