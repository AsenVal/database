﻿using System;
using System.Data;
using System.Data.OleDb;

namespace _06.ReadFromExcell
{
    class ReadFromExcel
    {
        static void Main()
        {
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
           @"Data Source=..\..\Scores.xlsx; Extended Properties=""Excel 12.0 Xml;HDR=YES""";

            OleDbConnection dbConn = new OleDbConnection(connectionString);

            // Open connection
            dbConn.Open();
            using (dbConn)
            {
                OleDbDataAdapter reader = new OleDbDataAdapter("SELECT * FROM [Sheet1$A1:B5]", dbConn);
                DataTable dt = new DataTable();
                reader.Fill(dt);
                foreach (DataRow item in dt.Rows)
                {
                    Console.WriteLine("{0} - {1}", item.ItemArray[0], item.ItemArray[1]);
                }
                Console.WriteLine();
            }

        }
    }
}
