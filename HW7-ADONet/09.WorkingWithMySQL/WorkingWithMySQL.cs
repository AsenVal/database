﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace _09.WorkingWithMySQL
{
    class WorkingWithMySQL
    {
        static void Main()
        {
            string conString = "Server=localhost; Port=3306; Database=Books; Uid=root; Pwd=; pooling=true";
            MySqlConnection dbConnection = new MySqlConnection(conString);

            ReadBooksInfo(dbConnection);
            SearchForBook("Pod Igoto", dbConnection);
            AddBook("New Book", new DateTime(2012, 5, 2), 226356, "Unknown", dbConnection);
        }

        private static void AddBook(string bookName, DateTime datePublish, int ISBN, string author, MySqlConnection dbConnection)
        {
            dbConnection.Open();
            string bookStr = "INSERT INTO books " +
          "(title, publishDate, ISBN) VALUES " +
          "(@title, @date, @isbn)";
            MySqlCommand addBook = new MySqlCommand(bookStr, dbConnection);
            addBook.Parameters.AddWithValue("@title", bookName);
            addBook.Parameters.AddWithValue("@date", datePublish);
            addBook.Parameters.AddWithValue("@isbn", ISBN);
            addBook.ExecuteNonQuery();

            MySqlCommand cmdSelectIdentity = new MySqlCommand("SELECT @@Identity", dbConnection);
            ulong insertedRecordId = (ulong)cmdSelectIdentity.ExecuteScalar();

            string authorStr = "INSERT INTO authors " +
                        "(bookId, authorName) VALUES " +
                        "(@bookId, @name)";
            MySqlCommand addAuthor = new MySqlCommand(authorStr, dbConnection);
            addAuthor.Parameters.AddWithValue("@bookId", (int)insertedRecordId);
            addAuthor.Parameters.AddWithValue("@name", author);
            addAuthor.ExecuteNonQuery();
            dbConnection.Clone();
        }

        private static void SearchForBook(string input, MySqlConnection dbConnection)
        {
            dbConnection.Open();
            string sqlStr = "USE books ; SELECT a.authorName, b.title, b.publishDate, b.ISBN FROM books b " +
                "JOIN authors a " +
                "ON a.bookId = b.bookId " +
                "WHERE title LIKE @input";
            MySqlParameter cmdParam = new MySqlParameter("@input", "%" + input + "%");
            MySqlCommand cmd = new MySqlCommand(sqlStr, dbConnection);
            cmd.Parameters.Add(cmdParam);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string author = (string)reader["authorName"];
                string title = (string)reader["title"];
                DateTime date = (DateTime)reader["publishDate"];
                int isbn = (int)reader["ISBN"];
                Console.WriteLine("{0}: {1} {2} {3}", author, title, date, isbn);
            }
            dbConnection.Close();
        }

        private static void ReadBooksInfo(MySqlConnection dbConnection)
        {
            dbConnection.Open();
            string sqlStr = "USE books ; SELECT a.authorName, b.title, b.publishDate, b.ISBN FROM books b " +
                "JOIN authors a " +
                "ON a.bookId = b.bookId";
            MySqlCommand cmd = new MySqlCommand(sqlStr, dbConnection);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string author = (string)reader["authorName"];
                string title = (string)reader["title"];
                DateTime date = (DateTime)reader["publishDate"];
                int isbn = (int)reader["ISBN"];
                Console.WriteLine("{0}: {1} {2} {3}", author, title, date, isbn);
            }
            dbConnection.Close();
        }
    }
}
