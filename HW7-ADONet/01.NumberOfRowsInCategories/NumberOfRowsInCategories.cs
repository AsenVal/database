﻿using System;
using System.Data.SqlClient;

namespace _01.NumberOfRowsInCategories
{
    public class NumberOfRowsInCategories
    {
        static void Main()
        {
            SqlConnection dbCon = new SqlConnection(Settings.Default.ServerString);
            dbCon.Open();
            using (dbCon)
            {
                SqlCommand command = new SqlCommand(
                    "SELECT COUNT(*) FROM Categories", dbCon);
                int numberOfRows = (int)command.ExecuteScalar();
                Console.WriteLine("Number of records in Category table");
                Console.WriteLine(numberOfRows);
            }
        }
    }
}
